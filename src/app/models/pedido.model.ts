import { Producto } from './producto.model';

export class Pedido{
    constructor(
        public id: number,
        public cliente: string,
        public productos: Producto[],
        public detalle: string,
        public alta: string
    ){}
}