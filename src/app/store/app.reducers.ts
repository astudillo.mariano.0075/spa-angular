
import * as reducers from './reducers';
import { ActionReducerMap } from '@ngrx/store';

export interface AppState{
    pedidos: reducers.PedidosState
}

export const appReducers: ActionReducerMap<AppState> = {
    pedidos: reducers.pedidosReducer
}

