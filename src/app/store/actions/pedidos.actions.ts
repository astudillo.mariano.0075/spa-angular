import { Action } from '@ngrx/store';
import { Pedido } from 'src/app/models/pedido.model';


export const LISTAR_PEDIDOS = '[Pedidos] Lista Pedidos';
export const LISTAR_PEDIDOS_FAIL = '[Pedidos] Lista Pedidos FAIL';
export const LISTAR_PEDIDOS_SUCCESS = '[Pedidos] Lista Pedidos SUCCESS';
export const ACTUALIZAR_PEDIDO = "[Pedidos] Actualizar Pedido";
export const ACTUALIZAR_PEDIDO_FAIL = "[Pedidos] Actualizar Pedido FAIL";
export const ACTUALIZAR_PEDIDO_SUCCESS = "[Pedidos] Actualizar Pedido SUCCESS";


export class ListarPedidos implements Action {
    readonly type = LISTAR_PEDIDOS;
}

export class ListarPedidosFail implements Action {
    readonly type = LISTAR_PEDIDOS_FAIL;

    constructor( public payload: any ){}
}

export class ListarPedidosSuccess implements Action {
    readonly type = LISTAR_PEDIDOS_SUCCESS;

    constructor( public pedidos: Pedido[] ){}
}

export class ActualizarPedido implements Action{
    readonly type = ACTUALIZAR_PEDIDO;
    constructor ( public pedido:Pedido[]  ){}
}

export class ActualizarPedidoFail implements Action{
    readonly type = ACTUALIZAR_PEDIDO_FAIL;
    constructor ( public payload: any  ){}
}

export class ActualizarPedidoSuccess implements Action{
    readonly type = ACTUALIZAR_PEDIDO_SUCCESS;
}

export type pedidosAcciones = ListarPedidos | ListarPedidosFail | ListarPedidosSuccess
                                | ActualizarPedido | ActualizarPedidoFail | ActualizarPedidoSuccess;