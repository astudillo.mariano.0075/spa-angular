import { Pedido } from 'src/app/models/pedido.model';
import * as fromPedidos from '../actions';

export interface PedidosState{
    pedidos: Pedido[],
    loaded: boolean,
    loading: boolean,
    error: any
}

const estadoInicial: PedidosState = {
    pedidos: [],
    loaded: false,
    loading: false,
    error: null
}

export function pedidosReducer( state = estadoInicial, action: fromPedidos.pedidosAcciones ){

    switch(action.type){

        case fromPedidos.LISTAR_PEDIDOS:
            return{
                ...state,
                loading:true
            };
        
        case fromPedidos.LISTAR_PEDIDOS_SUCCESS:
            return{
                ...state,
                loading: false,
                loaded: true,
                pedidos: [...action.pedidos]
            }

        case fromPedidos.LISTAR_PEDIDOS_FAIL:
            return{
                ...state,
                loading: false,
                loaded: false,
                error: {
                    status: action.payload.status,
                    message: action.payload.message,
                    url: action.payload
                }
            }

        case fromPedidos.ACTUALIZAR_PEDIDO:
            return{
                ...state
            }

        case fromPedidos.ACTUALIZAR_PEDIDO_SUCCESS:
            return{
                ...state
            }
    
        case fromPedidos.ACTUALIZAR_PEDIDO_FAIL:
            return{
                ...state,
                error: [...action.payload]
            }

        default:
            return state;
    }

}
