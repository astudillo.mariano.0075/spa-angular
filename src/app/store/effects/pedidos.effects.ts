import { Injectable } from '@angular/core';
import { Actions , Effect, ofType, createEffect } from '@ngrx/effects';
import { EMPTY , of } from 'rxjs';
import { map, mergeMap, catchError, switchMap } from 'rxjs/operators';
import { PedidosService } from '../../services/pedidos.service';
import * as pedidosActions from '../actions';
import { Pedido } from 'src/app/models/pedido.model';

@Injectable()
export class PedidosEffects {

    constructor(
        private actions$: Actions,
        private servicePedidos: PedidosService
    ){}s

    @Effect()
    listarPedidos$ = this.actions$.pipe(
        ofType(pedidosActions.LISTAR_PEDIDOS),
        switchMap(() => {
            return this.servicePedidos.getPedidos()
                .pipe( 
                    map( (pedidos:any) => new pedidosActions.ListarPedidosSuccess(pedidos) ),
                    catchError( error => of(new pedidosActions.ListarPedidosFail(error)) )
                );
        })
    )


    @Effect()
    actualizaPedido$ = this.actions$.pipe(
        ofType(pedidosActions.ACTUALIZAR_PEDIDO),
        switchMap((pedido:any) => {
            return this.servicePedidos.actualizarPedido(pedido)
                .pipe(
                    map( (pedidos:any) => new pedidosActions.ActualizarPedidoSuccess() ),
                    catchError( (error) => of( new pedidosActions.ActualizarPedidoFail(error) ) )
                );
        })
    )

}