import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// === NEW MODULES ===
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// === TABLE REORDER MODULES ===
import {DragDropModule} from '@angular/cdk/drag-drop';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';

// === MATERIAL MODULES ===
import * as fromMatModule from '@angular/material';

// === COMPONENTS ===
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { ModalDetalleComponent } from './components/pedidos/modal-detalle/modal-detalle.component';


// === NGRX ===
import { StoreModule } from '@ngrx/store';
import { appReducers } from './store/app.reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { PedidosEffects } from './store/effects/index';
import { effectsArr } from './store/effects';

@NgModule({
  declarations: [
    AppComponent,
    PedidosComponent,
    ModalDetalleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    fromMatModule.MatToolbarModule,
    fromMatModule.MatSidenavModule,
    fromMatModule.MatListModule,
    fromMatModule.MatIconModule,
    fromMatModule.MatCardModule,
    fromMatModule.MatTableModule,
    fromMatModule.MatPaginatorModule,
    fromMatModule.MatFormFieldModule,
    fromMatModule.MatInputModule,
    fromMatModule.MatSortModule,
    fromMatModule.MatPaginatorModule,
    fromMatModule.MatDialogModule,
    fromMatModule.MatFormFieldModule,
    fromMatModule.MatInputModule,
    fromMatModule.MatButtonModule,
    fromMatModule.MatListModule,
    CdkTableModule, CdkTreeModule, DragDropModule,
    ReactiveFormsModule,
    FormsModule, 
    StoreModule.forRoot( appReducers ),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
    ,EffectsModule.forRoot( effectsArr )
  ],
  entryComponents: [
    ModalDetalleComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
