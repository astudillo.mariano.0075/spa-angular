import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// === NEW COMPONENTS ===
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {path: '', component: PedidosComponent},
  {path: 'pedidos', component: PedidosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
