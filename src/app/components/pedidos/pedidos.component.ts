// === MODULES ===
import { CdkDragStart, CdkDropList, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

// === COMPONENTS ===
import { ModalDetalleComponent } from './modal-detalle/modal-detalle.component';

// === SERVICES ===
import { PedidosService } from '../../services/pedidos.service';
import { AppState } from 'src/app/store/app.reducers';

// === NGRX ===
import { Store } from '@ngrx/store';
import * as pedidosActions from '../../store/actions';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})

export class PedidosComponent implements OnInit {

  //====================== VARIABLES ======================
  
  //Tabla
  private columns: any[] = [ { field: 'id' }, { field: 'cliente' }, { field: 'zona' }, { field: 'alta' }]; //Columnas de Tabla
  private displayedColumns: string[] = [];
  private previousIndex: number;
  private tableDefaultRows: number = 20; //Filas por defecto a Mostrar

  //Pedidos
  private pedidos = new MatTableDataSource();

  @ViewChild(MatSort, {static: true}) sort: MatSort; //Ordena Tabla
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator; //Paginador Tabla

  //====================== CONSTRUCTOR ======================
  constructor( public servicePedidos: PedidosService ,
    public dialog:MatDialog ,
    public store: Store<AppState> ) { }

  ngOnInit() {
    // this.loadPedidos();
    this.loadPedidos();
    this.setDisplayedColumns();
  }


  //====================== FUNCTIONS ======================
  //Funcion que llama a Service que obtiene los pedidos
  public loadPedidos(){
    this.store.dispatch( new pedidosActions.ListarPedidos() )
    this.store.select('pedidos').subscribe( (data) => {
      this.pedidos.data = data.pedidos; //Asigna valores a Origen de Datos de Tabla
      this.pedidos.paginator = this.paginator; //Asigna paginador a los pedidos
      this.pedidos.sort = this.sort; //Asigna ordenador a los pedidos
    });
  }


  //Funcion para filtrar los pedidos por el valor del input en el evento KEYUP
  private filterPedidos(event: Event){
    let filterValue = (event.target as HTMLInputElement).value; //Toma valor de form
    this.pedidos.filter = filterValue.trim().toLowerCase(); //Filtra por el valor
  }

  //Funcion que se ejecuta al seleccionar una fila de la tabla
  private rowClicked(data){
    this.dialog.open(ModalDetalleComponent, { width: '100%', panelClass: "custom-class-modal" ,data: {pedido: data} });
  }

  //Grupo de Funciones para manejar el reordenamiento de Columnas
  private setDisplayedColumns() {
    this.columns.forEach(( colunm, index) => {
      colunm.index = index;
      this.displayedColumns[index] = colunm.field;
    });
  }
  private dragStarted(event: CdkDragStart, index: number ) {
    this.previousIndex = index;
  }
  private dropListDropped(event: CdkDropList, index: number) {
    if (event) {
      moveItemInArray(this.columns, this.previousIndex, index);
      this.setDisplayedColumns();
    }
  }
}
