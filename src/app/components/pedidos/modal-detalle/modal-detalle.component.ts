import { Component, OnInit, Inject } from '@angular/core';

// === MODULES ===
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatInput , MatFormField , MatList } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';

// === NGRX ===
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducers';
import * as pedidosActions from '../../../store/actions'; 

@Component({
  selector: 'app-modal-detalle',
  templateUrl: './modal-detalle.component.html',
  styleUrls: ['./modal-detalle.component.scss']
})

export class ModalDetalleComponent implements OnInit {

  private pedido: any;
  private formPedido: any = FormGroup;

  constructor ( public dialogRef: MatDialogRef<ModalDetalleComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any, public formBuilder: FormBuilder, public store: Store<AppState>){
    this.pedido = data["pedido"];
    this.formPedido = this.formBuilder.group({
      detalle : this.pedido.detalle
    });
  }

  ngOnInit(){
    
  }

  //Evento Click Cancelar
  private clickCancel(){
    this.dialogRef.close();
  }

  //Evento Click Guardar
  public submitFormularioPedido(){
    this.pedido.detalle = this.formPedido.value.detalle;
    this.store.dispatch( new pedidosActions.ActualizarPedido(this.pedido) );
    this.dialogRef.close();
  }

}
