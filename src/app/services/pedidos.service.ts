import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';


import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  private SERVIDOR = "http://localhost:3000"; //Servidor API

  constructor(public http: HttpClient) {}

  //Lista toda los Pedidos
  public getPedidos(){
    let url = this.SERVIDOR + '/pedidos';
    return this.http.get(url).pipe(
      map(data => { return data })
    );
  }

  //Actualiza un Pedido por ID
  public actualizarPedido(data){
    let new_pedido = data.pedido;
    let url = this.SERVIDOR + '/pedidos/' + new_pedido.id; 
    let options = {
      headers : new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }

    return this.http.put(url, new_pedido, options).pipe(
      map(data => { return data })
    );
  }

}
