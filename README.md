# SpaAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.16.

Autor: Mariano Astudillo

## Servidor de Desarollo

En linea de comandos dentro del proyecto ejecutar los siguientes comandos:

####1) Instalar globalmente paquete de json-server

`npm install -g json-server`

####2) Generar node_modules

`npm install`

####3) En una segunda ventana de consola. Levantar Mock Server

`json-server --watch db.json`

####4) En la primer ventana de consola. Iniciar Cliente de App

`ng serve -o`

